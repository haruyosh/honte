# honte - yet another tweet filtering system
honte aims to filter any tweets with keywords using Streaming API (or others).  All selected tweets are put into SQS queue and taken them into DynamoDB asyncronously.

Currently honte comprises searcher and viewer.  Searcher acturizes above tweets filtering and DB putting function.  Viewer provides rough-realtime view for DB data.

## searcher
searcher has two major scripts, tw_to_sqs.py and sqs_to_ddb_long.py .  Each script can run independently.  Required to prepare one SQS queue and one DynamoDB table.

### SQS Queue
It's be great to set "Receive Message Wait Time" to 20 seconds.

### DynamoDB table schema
Assumed to have following schema.  Actually DynamoDB requires only the top 2 records (Primary keys) when creating new Table.

| Cols	     | Attributes	| Description	|
|------------|----------------------|------------------|
| keyword    | String(Primary Hash)	| Filtered keyword |
| created_at | Number(Primary Range)	| ['created_at'] described as epoch |
| twid	     | Number	      | ['id'] |
| userid     | Number 	      | ['user']['id'] |
| text	     | String	      | ['text'] |
| data	     | String	      | all dumped tweet as json data |

tweet's entities used in above table should be refered to this document: https://dev.twitter.com/docs/entities

### tw_to_sqs.py
This script filters tweets from the stream provided Streaming API currently and put them into SQS queue.

Firstly you have to make twtokens.py to specify your twitter API access tokens.  See tw_to_sqs.py directly to get example.  Also you can customize trackwords.py according to your own needs.

### sqs_to_ddb_long.py
This script takes tweets from SQS queue and put into DynamoDB.

## viewer
Currently this component only show a part of data in accordance with specified keywords.  It can work with apache mod_php5 environment.
