<?php
error_reporting(E_ALL);
require_once('vendor/autoload.php');
require_once('cred.php');
use Aws\Common\Aws;
use Aws\DynamoDb\DynamoDbClient;

/*
cred.php shold provide AWS Credentials like this:
$config = array(
'key' => 'AKIxxxx',
'secret' => 'yyyy',
'region' => 'us-west-2'
);
 */

$arg1 = $_GET['arg1'];
//$arg1 = 'JAWS';

$aws = Aws::factory($config);
$client = $aws->get('DynamoDb');

//$result = $client->describeTable(array('TableName' => 'mytable'));
//echo $result

$response = $client->query(
  array(
    'TableName' => 'twtabledev',
    'KeyConditions' => array(
      'keyword' => array(
	'AttributeValueList' => array(
	  array('S' => $arg1)),
	'ComparisonOperator' => 'EQ'
      )
    )
  )
);
?>

<html><head></head>
<body>

<h1>Result:
<?php echo $response['Count']; ?>
records</h1>
<div>
<table>
<?php foreach($response['Items'] as $i) : ?>
<tr>
<td>
<?php
echo "\n";
//$dt = new DateTime($i['created_at']['N']);
//echo htmlspecialchars($dt->format('Y-m-d H:i:s'), ENT_QUOTES, 'utf-8')
echo htmlspecialchars(date('Y-m-d H:i:s',$i['created_at']['N']), ENT_QUOTES, 'utf-8')
?>
</td>
<td><?php echo htmlspecialchars($i['text']['S'], ENT_QUOTES, 'utf-8')?></td>
</tr>
<?php endforeach; ?>
</table>
<p><a href="query.html">戻る</a></p>
</body>
</html>
