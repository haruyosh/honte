from datetime import datetime
import json, re, sys

import boto
from boto.sqs.message import Message
from boto.dynamodb2.fields import HashKey, RangeKey, AllIndex
from boto.dynamodb2.table import Table
from boto.dynamodb2.types import NUMBER

import trackwords

readthreshold = 8
pollsec = 20

def parse_created_at(created_at_str) :
    twfmtstr = "%a %b %d %H:%M:%S +0000 %Y"
    return int(datetime.strptime(created_at_str, twfmtstr).strftime("%s"))

def put_item_to_table(table_table, record_list, keyword_str) :
    print keyword_str + '\t' + record_list['created_at'] + '\t' + str(record_list['id'])

    # SHOULD BE FIXED
    # if already exists then it should be RTs
    for r in table_table.query(twid__eq=record_list['id'], index='twid-index'):
        print r.get(keyword_str)
        sys.stderr.write("**** Retweet ****")
        # SHOULD BE FIXED
        # Could sum up the counter
        return

    try:
        table_table.put_item(data={
                'keyword': keyword_str,
                'created_at': parse_created_at(record_list['created_at']),
                'twid': record_list['id'],
                'userid': record_list['user']['id'],
                'text': record_list['text'],
                'data': json.dumps(record_list)})
    except Exception as e:
        print str(type(e))
        print str(e.args)
        print e.message
        print str(e)
        

if __name__ == '__main__':
    theqconn = boto.sqs.connect_to_region('us-west-2')
    thequeue = theqconn.get_queue('twqdev')
    thecount = 0
    thehandling = 0
    thetable = Table('twtabledev')

    thequeue.set_attribute('ReceiveMessageWaitTimeSeconds',pollsec)
    sys.stderr.write("%s\n" % "Connected to the queue.")

    while 1:
        records = []

        # To delete_message, Message Receipt must be used the last one's
        uniqbodys = {}

        # Freeze the current number of messages
        thecount = thequeue.count()

        # Reading max should be smaller than readthreshold
        # if  thecount >= readthreshold:
        #     thehandling = readthreshold
        # elif thecount == 0:
        #     thehandling = 1

        sys.stderr.write("Reading the queue with long polling with %s secs...\n" % pollsec)
        # msgs = thequeue.get_messages(thehandling,5,'',pollsec)
        msgs = thequeue.get_messages(readthreshold, 5, '', pollsec)
        sys.stderr.write("Got Messages: %s / %s\n" % (len(msgs), thecount))

        for m in msgs:
            sys.stderr.write("%s\n" % json.loads(m.get_body())['id'])

            # Message bodys can be use the first one
            if not uniqbodys.has_key(m.MD5OfBody):
                uniqbodys[m.MD5OfBody] = m.get_body()

            # Delete everytime
            thequeue.delete_message(m)

        records = [json.loads(body) for body in uniqbodys.values()]

        sys.stderr.write("Received unique Messages: %s / %s\n" % (len(records), thecount))
        for twjson in records:
            # uniq for regexp keywords: 'AWS AWS aws EC2 ec2' -> 'AWS aws EC2 ec2'
            matched = list(set(re.findall(trackwords.twordsregexp,twjson['text'])))
            # uniq for regexp keywords regardless capital:
            # 'AWS aws EC2 ec2' -> 'AWS EC2'
            matched = list(set([w.upper() for w in matched]))
#            sys.stderr.write("%s\t%s\n" % (twjson['id'], matched))

            for kw in matched:
                put_item_to_table(thetable, twjson, kw)
                sys.stderr.write("%s %s\t  %s\n" % (twjson['id'],  kw, twjson['text']))
        dt = datetime.today().strftime('%Y/%m/%d %H:%M:%S')
        sys.stderr.write("%s Polling loop...\n" % (dt))
        sys.stderr.write("%s\n" % '----------------')
