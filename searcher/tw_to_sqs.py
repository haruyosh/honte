import json, re, sys
import boto.sqs
from boto.sqs.message import Message
from tweepy import OAuthHandler, Stream
from tweepy.streaming import StreamListener

import trackwords
import twtokens

# The following block should be provided as twtokens.py:
# Consumer keys and access tokens, used for OAuth
# consumer_key = 'aaaa'
# consumer_secret = 'bbbb'
# access_token = 'cccc'
# access_token_secret = 'dddd'

class SQSListener(StreamListener):
    def __init__(self, queue):
        self.q = queue
        self.i = 0
    def on_data(self, data):
        if data.startswith('{'):
            self.i = self.i+1
            sys.stderr.write("%s\t%s\n" %  (str(self.i),str(self.q.count())))
            twjson = json.loads(data)
            if 'text' in twjson:
                matched = list(set(re.findall(trackwords.twordsregexp,twjson['text'])))
                # sys.stderr.write("%s\n" % matched)
                if len(matched) > 0:
                    for kw in matched:
                        sys.stderr.write("%s %s\t  %s\n" % (twjson['id'],  kw, twjson['text']))
                        m = Message()
                        m.set_body(data)
                        self.q.write(m)
            sys.stderr.write("%s\n" % '----------------')
        else:
            sys.stderr.write("%s\t%s\n" % ('Non-json:', data))
        return True
    def on_error(self, status):
        sys.stderr.write("%s\n" % status)

if __name__ == '__main__':
    auth = OAuthHandler(twtokens.consumer_key, twtokens.consumer_secret)
    auth.set_access_token(twtokens.access_token, twtokens.access_token_secret)

    theqconn = boto.sqs.connect_to_region('us-west-2')
    thequeue = theqconn.get_queue('twqdev')

    listener = SQSListener(thequeue)
    stream = Stream(auth, listener)
    stream.filter(track=trackwords.twords)
